function varargout = test(varargin)
% TEST MATLAB code for test.fig
%      TEST, by itself, creates a new TEST or raises the existing
%      singleton*.
%
%      H = TEST returns the handle to a new TEST or the handle to
%      the existing singleton*.
%
%      TEST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TEST.M with the given input arguments.
%
%      TEST('Property','Value',...) creates a new TEST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before test_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to test_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help test

% Last Modified by GUIDE v2.5 14-Dec-2016 18:38:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @test_OpeningFcn, ...
                   'gui_OutputFcn',  @test_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before test is made visible.
function test_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to test (see VARARGIN)

% Choose default command line output for test
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes test wait for user response (see UIRESUME)
% uiwait(handles.figure_test);

setappdata(handles.figure_test, 'img_src', 0);  %原图矩阵
setappdata(handles.figure_test, 'img_dst', 0);  %目标图像矩阵

setappdata(handles.figure_test, 'filename', '');  %存储读入的图片的name便于保存时使用
setappdata(handles.figure_test, 'filepath', '');

setappdata(handles.figure_test,'bSave', false);
setappdata(handles.figure_test,'bChanged', false);
setappdata(handles.figure_test,'fstSave', true);
setappdata(handles.figure_test, 'fstPath', 0);

set(handles.filter, 'Enable', 'off');


%关于画笔
set(handles.m_pen, 'Enable', 'off');  %画笔可用开关
setappdata(handles.figure_test, 'eflag', false);  %画笔标志位
% userful color str: 'blue', 'green', 'red', 'cyan', 'magenta', 'black','black',and 'white'
setappdata(handles.figure_test, 'pencolor', [0 0 0]); %画笔颜色
setappdata(handles.figure_test, 'linewidth',10);
setappdata(handles.figure_test, 'btdown', false); %鼠标是否按下的标志
setappdata(handles.figure_test, 'pt_start', 0); %画笔起始位置
setappdata(handles.figure_test, 'pt_end', 0); %画笔结束位置

setappdata(handles.figure_test, 'img_line', 0); %存储划线图片的矩阵

setappdata(handles.figure_test, 'line_pos', 0); %存储划线路径的矩阵

% 保存按键的可用性
set(handles.m_file_save_origin, 'Enable', 'off');  %保存原图可用性开关
set(handles.m_file_save_final, 'Enable', 'off');  %保存目标图片可用开关

% --- Outputs from this function are returned to the command line.
function varargout = test_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function m_file_Callback(hObject, eventdata, handles)
% hObject    handle to m_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function m_file_open_Callback(hObject, eventdata, handles)
% hObject    handle to m_file_open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename,pathname]=uigetfile(...
{'*.bmp;*.jpg;*.png;*.jpeg','ImageFiles(*.bmp,*.jpg,*.png,*.jpeg)';...
'*.*','AllFiles(*.*)'},...
'Pickanimage');
if isequal(filename, 0) || isequal(pathname, 0)
    return;
end
axes(handles.axes_src);  %用axes命令设定当前操作的坐标轴是axes_src
fpath=[pathname filename]; %将文件名和目录名组合成一个完整的路径
img_src=imread(fpath);
imshow(img_src); %用imread读入图片，并用imshow在axes_src上显示

setappdata(handles.figure_test, 'filename', filename);  %存储读入的图片的name便于保存时使用
setappdata(handles.figure_test, 'filepath', pathname);

setappdata(handles.figure_test, 'img_src', img_src); 
setappdata(handles.figure_test, 'img_dst', img_src); 
% setappdata(handles.figure_test, 'img_line', ones(size(img_src)));  %全1矩阵，白底
img_line = zeros(size(img_src));
img_line(:,:,1)=0.5;
img_line(:,:,2)=0.5;
img_line(:,:,3)=0.5;
setappdata(handles.figure_test, 'img_line', img_line); %灰色底

set(handles.m_file_save_origin,'Enable','on');
set(handles.m_pen, 'Enable', 'on');
set(handles.filter, 'Enable', 'on');



% --------------------------------------------------------------------
function m_file_save_origin_Callback(hObject, eventdata, handles)
% hObject    handle to m_file_save_origin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_src = getappdata(handles.figure_test, 'img_src'); %取得打开图片的数据
defaultName = getappdata(handles.figure_test, 'filename');
[filename, pathname] = uiputfile({'*.png', '*.PNG files';'*.bmp','*.BMP files'; '*.jpg', 'JPG files'}, 'Pick an Image', defaultName);
if isequal(filename, 0) || isequal(pathname, 0)
    return ; %如果点了取消
else
    fpath = fullfile(pathname, filename); %获取全路径的另一种方法
end
imwrite(img_src, fpath); %保存图片


% --------------------------------------------------------------------
function m_file_save_final_Callback(hObject, eventdata, handles)
% hObject    handle to m_file_save_final (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_dst = getappdata(handles.figure_test, 'img_dst'); %取得图片的数据
defaultName = getappdata(handles.figure_test, 'filename');
[filename, pathname] = uiputfile({'*.png', '*.PNG files';'*.bmp','*.BMP files'; '*.jpg', 'JPG files'}, 'Pick an Image', defaultName);
if isequal(filename, 0) || isequal(pathname, 0)
    return ; %如果点了取消
else
    fpath = fullfile(pathname, filename); %获取全路径的另一种方法
end
imwrite(img_dst, fpath); %保存图片


% --------------------------------------------------------------------
function m_file_save_line_Callback(hObject, eventdata, handles)
% hObject    handle to m_file_save_line (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_line = getappdata(handles.figure_test, 'img_line'); %取得图片的数据
defaultName = getappdata(handles.figure_test, 'filename');
[filename, pathname] = uiputfile({'*.png', '*.PNG files';'*.bmp','*.BMP files'; '*.jpg', 'JPG files'}, 'Pick an Image', defaultName);
if isequal(filename, 0) || isequal(pathname, 0)
    return ; %如果点了取消
else
    fpath = fullfile(pathname, filename); %获取全路径的另一种方法
end
imwrite(img_line, fpath); %保存图片


% --------------------------------------------------------------------
function m_file_exit_Callback(hObject, eventdata, handles)
% hObject    handle to m_file_exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bChanged = getappdata(handles.figure_test, 'bChanged');%获得是否更改
bSave = getappdata(handles.figure_test, 'bSave'); %获得是否保存
if bChanged == true && bSave==false
    btnName=questdlg('您已经更改了图片，但没有保存。要保存吗？', '提示', '保存','不保存','保存'); %用提问对话框
    switch btnName
        case '保存',  %执行axes_dst_menu_save_Callback的功能
%             feval(@axes_dst_menu_save_Callback, handles.axes_dst_menu_save,eventdata,handles);
        case '不保存', %什么也不做
    end
end
close(findobj('Tag', 'figure_test'));


% --------------------------------------------------------------------
function m_pen_Callback(hObject, eventdata, handles)
% hObject    handle to m_pen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_dst = getappdata(handles.figure_test, 'img_dst');
axes(handles.axes_dst);
imshow(img_dst);
set(handles.m_file_save_final, 'Enable', 'on');  %保存目标图片可用开关


% --------------------------------------------------------------------
function m_pen_white_Callback(hObject, eventdata, handles)
% hObject    handle to m_pen_white (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(handles.figure_test, 'eflag', true);
setappdata(handles.figure_test, 'pencolor', 'white');



% --------------------------------------------------------------------
function m_pen_black_Callback(hObject, eventdata, handles)
% hObject    handle to m_pen_black (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(handles.figure_test, 'eflag', true);
setappdata(handles.figure_test, 'pencolor', 'black');


% --------------------------------------------------------------------
function m_pen_blue_Callback(hObject, eventdata, handles)
% hObject    handle to m_pen_blue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(handles.figure_test, 'eflag', true);
setappdata(handles.figure_test, 'pencolor', 'blue');


% --------------------------------------------------------------------
function m_pen_green_Callback(hObject, eventdata, handles)
% hObject    handle to m_pen_green (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(handles.figure_test, 'eflag', true);
setappdata(handles.figure_test, 'pencolor', 'green');


% --------------------------------------------------------------------
function m_pen_red_Callback(hObject, eventdata, handles)
% hObject    handle to m_pen_red (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(handles.figure_test, 'eflag', true);
setappdata(handles.figure_test, 'pencolor', 'red');




% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure_test_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 if strcmp(get(gcf,'SelectionType'),'normal') % 通过比较得到何种鼠标响应 normal鼠标左键alt鼠标右键
       setappdata(handles.figure_test, 'btdown', true);
       pt_start = get(handles.axes_dst, 'CurrentPoint');
       setappdata(handles.figure_test, 'pt_start', pt_start);
 end


% --- Executes on mouse motion over figure - except title and menu.
function figure_test_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to figure_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

btdown = getappdata(handles.figure_test, 'btdown');
eflag = getappdata(handles.figure_test, 'eflag');
pencolor = getappdata(handles.figure_test, 'pencolor');
pt_start = getappdata(handles.figure_test, 'pt_start');
img_dst=getappdata(handles.figure_test, 'img_dst');
line_pos = getappdata(handles.figure_test, 'line_pos');
axes(handles.axes_dst);

if (btdown==true)&&(eflag==true) %鼠标左键按下且选择了一种画笔颜色
     pt_end = get(handles.axes_dst,'CurrentPoint'); %获取当前鼠标位置坐标
     setappdata(handles.figure_test, 'pt_end', pt_end);
     if(line_pos==false)
         line_pos = [pt_start(1,1), pt_start(1,2),  pt_end(1,1), pt_end(1,2)];
         setappdata(handles.figure_test, 'line_pos',line_pos);
     else
         [row, col] = size(line_pos);
         line_pos(row+1,:)  =[pt_start(1,1), pt_start(1,2),  pt_end(1,1), pt_end(1,2)];
         setappdata(handles.figure_test, 'line_pos',line_pos);
     end
%      line([pt_start(1,1) pt_end(1,1)], [pt_start(1,2) pt_end(1,2)], 'linewidth', 4, 'color', pencolor); %划线
    linewidth = getappdata(handles.figure_test, 'linewidth');
     img_temp = insertShape(img_dst, 'Line',line_pos, 'LineWidth',linewidth,'Color', pencolor);
     setappdata(handles.figure_test, 'img_dst', img_temp);
     setappdata(handles.figure_test, 'pt_start', pt_end);
     imshow(img_temp);
     
     img_line = getappdata(handles.figure_test, 'img_line');
     img_line = insertShape(img_line, 'Line',line_pos, 'LineWidth',linewidth,'Color', pencolor);
     setappdata(handles.figure_test, 'img_line', img_line);
end


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure_test_WindowButtonUpFcn(hObject, eventdata, handles)
% hObject    handle to figure_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(handles.figure_test, 'btdown', false);
setappdata(handles.figure_test, 'line_pos', 0);



function pen_edit_linewidth_Callback(hObject, eventdata, handles)
% hObject    handle to pen_edit_linewidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pen_edit_linewidth as text
%        str2double(get(hObject,'String')) returns contents of pen_edit_linewidth as a double
linewidth =  str2double(get(hObject,'String'));
setappdata(handles.figure_test, 'linewidth', linewidth);


% --- Executes during object creation, after setting all properties.
function pen_edit_linewidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pen_edit_linewidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in m_clear.
function m_clear_Callback(hObject, eventdata, handles)
% hObject    handle to m_clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_src = getappdata(handles.figure_test, 'img_src');
setappdata(handles.figure_test, 'img_dst', img_src); 
% setappdata(handles.figure_test, 'img_line', ones(size(img_src)));  %全1矩阵，白底
img_line = zeros(size(img_src));
img_line(:,:,1)=0.5;
img_line(:,:,2)=0.5;
img_line(:,:,3)=0.5;
setappdata(handles.figure_test, 'img_line', img_line); %灰色底

img_dst = getappdata(handles.figure_test, 'img_dst');
axes(handles.axes_dst);
imshow(img_dst);


% --------------------------------------------------------------------
function filter1_Callback(hObject, eventdata, handles)
% hObject    handle to filter1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_src = getappdata(handles.figure_test, 'img_src');
img_fil=imnoise(img_src,'salt & pepper',0.02);
axes(handles.axes_dst);
imshow(img_fil);
setappdata(handles.figure_test, 'img_dst', img_fil1);


% --------------------------------------------------------------------
function filter2_Callback(hObject, eventdata, handles)
% hObject    handle to filter2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_src = getappdata(handles.figure_test, 'img_src');
img_fil=rgb2gray(img_src);
axes(handles.axes_dst);
imshow(img_fil);
setappdata(handles.figure_test, 'img_dst', img_fil);


% --------------------------------------------------------------------
function filter3_Callback(hObject, eventdata, handles)
% hObject    handle to filter3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_src = getappdata(handles.figure_test, 'img_src');
w=[1 1 1;
        1 -4 1;
        1 1 1];
img_fil=imfilter(img_src,w,'replicate');
axes(handles.axes_dst);
imshow(img_fil);
setappdata(handles.figure_test, 'img_dst', img_fil);


% --- Executes on button press in AddLight.
function AddLight_Callback(hObject, eventdata, handles)
% hObject    handle to AddLight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_dst = getappdata(handles.figure_test, 'img_dst');
img_fil=imadjust(img_dst,[],[],0.5);
axes(handles.axes_dst);
imshow(img_fil);
setappdata(handles.figure_test, 'img_dst', img_fil);


% --- Executes on button press in DecreaseLight.
function DecreaseLight_Callback(hObject, eventdata, handles)
% hObject    handle to DecreaseLight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_dst = getappdata(handles.figure_test, 'img_dst');
img_fil=imadjust(img_dst,[],[],2);
axes(handles.axes_dst);
imshow(img_fil);
setappdata(handles.figure_test, 'img_dst', img_fil);
